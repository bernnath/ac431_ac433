close all
clear all
%% Data
Fe = 50;
Te = 1/Fe;
z = tf('z',Te);
%Bprocede=[0 0 0 0 0.0298 0.0588];
%Aprocede=[0 1 -1.3914 0.4696 0 0];
Bprocede=[0.725 -0.1575];
Aprocede=[1 -0.2707 0.0127];

Hprocede = tf(Bprocede,Aprocede,Te);
[A, B, C, D] = tf2ss(Hprocede.num{1}, Hprocede.den{1});

%% System aug

[A_a,B_a,C_a,D_a,Q_c]=sys_aug(A,B,C,D);

%% alpha et beta argu fonction
% 2 fonction 1 L et une K
alpha = [1 10 100 1 10 10 100];
beta = [1 10 100 10 100 1 10];


for i = 1:length(alpha)
%alpha doit garantir la meilleure fonction de sensibilité

K_a = gain_correcteur(A_a,B_a,Q_c,alpha(i));
% dualité

L = gain_observateur(A,B,C,beta(i));

%fonction de sensibilité 1 fonction avec arg loop
 dim_sys = length(B);
 [Nloop,Dloop]=ss2tf(A_a-B_a*K_a, B_a, C_a, D_a);
 loop = tf(Nloop,Dloop);
 [sensi_a, ~] = Sensibilite(loop);
%max_a=max(bode(sensi_a));
%MM_a = 1/max_a;


% RST
[R,S,T]=rst(A,B,C,K_a,L);
K=tf(R,S,Te);
looprst = Hprocede*K;
[sensi_rst, sensi_compl_rst] = Sensibilite(looprst);
F=tf(T,S,Te);
H_BO_rst=F*Hprocede;
H_BF_rst=F*Hprocede/(1+Hprocede*K);
[y,tOut] = step(H_BF_rst);

%
%max=max(bode(sensi_rst));
%MM = 1/max;
% Script de base
figure(1)
hold on
dbode(A_a,B_a,K_a,D_a,Te);
title("BO sys aug")
legend("1 1","10 10","100 100","1 10","10 100","10 1","100 10");
figure(2)
hold on
bode(sensi_a)
title("sensi sys aug");
legend("1 1","10 10","100 100","1 10","10 100","10 1","100 10");
figure(3)
hold on
bode(sensi_rst);
legend("sensi sys rst");
figure(4)
hold on
bode(sensi_compl_rst,logspace(-1,2,10000));
title("sensi compl sys rst");
legend("1 1","10 10","100 100","1 10","10 100","10 1","100 10");
figure(5)
hold on
sigma(H_BF_rst,Hprocede,logspace(-1,2,10000));
legend("bf sys aug", "bo procédé");
figure(6)
hold on
bode(H_BO_rst);
title("BO rst");
legend("1 1","10 10","100 100","1 10","10 100","10 1","100 10");
figure(7)
hold on
plot(tOut,y);
title("Réponse à un échelon");
legend("1 1","10 10","100 100","1 10","10 100","10 1","100 10");


figure()
nyquist(H_BF_rst)
end
