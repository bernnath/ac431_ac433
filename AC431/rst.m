function [R, S, T] = rst(A,B,C,K_a,L)
dim_sys = length(B);

Arst = [[A-B*K_a(1,1:dim_sys)-L*C -B*K_a(1,dim_sys+1)];[zeros(1,dim_sys) 1]];
[T,S]=ss2tf(Arst,[zeros(dim_sys,1);-1],-K_a,0);
[R,S]=ss2tf(Arst,[L;1],-K_a,0);
R=-R;
