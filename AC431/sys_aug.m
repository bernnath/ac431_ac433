function [A_a, B_a, C_a, D_a, Q_c] = sys_aug(A,B,C,D)
dim_sys = length(B);
A_a = [[A; C] [zeros(dim_sys,1);1]];
B_a = [B; 0];
C_a = [C 0];
D_a = D;

Q_c = [[C'*C zeros(dim_sys,1)];[zeros(1,dim_sys) 1]];
