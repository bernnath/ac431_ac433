function [L] = gain_observateur(A,B,C,beta)
L = (dlqr(A',C',B*B',beta))';