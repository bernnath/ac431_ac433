function [K] = gain_correcteur(A,B,Q,alpha)
K = dlqr(A,B,Q,alpha);