%Système
A=[1 -1.5 0.7];
B=[0 1 0.5];
Ts=1;
M=idpoly(A,B,Ts);

%Entrée
N=1000;
U = idinput(N);

%Sortie
SimOut = sim(M,U);
plot(SimOut);

%Réponse impulsionelle
I=impulse(M,0:N);

%% Etude de l'influence du nombre d'échantillons de mesure
N=[100 500 1000];
E=[0 0 0];
for i=1:3
    U=idinput(N(i));
    SimOut=sim(M,U);
    Z=iddata(SimOut,U,Ts);
    Sys_estim=cra(Z);
    I=impulse(M,0:N(i));
    figure(i)
    subplot(2,1,1)
    plot(I);
    subplot(2,1,2)
    plot(Sys_estim);
    for j=1:length(Sys_estim)
        E(i)=E(i)+abs(I(j)-Sys_estim(j));
    end
    E(i)=E(i)/N(i);
end
% Comme on s'y attend quand on a plus de point on est plus précis

%% Etude de l'influence de la variance du bruit
N=1000;
e=[0.1 0.5 1 5];
Ebruit=[0 0 0 0];
for i=1:length(e)
    Mbruit=idpoly(A,B,1,1,1,e(i));
    U=idinput(N);
    SimOut=sim(Mbruit,U);
    Z=iddata(SimOut,U,Ts);
    Sys_estim=cra(Z);
    I=impulse(Mbruit,0:N);
    figure(i)
    subplot(2,1,1)
    plot(I);
    subplot(2,1,2)
    plot(Sys_estim);
    for j=1:length(Sys_estim)
        Ebruit(i)=Ebruit(i)+abs(I(j)-Sys_estim(j));
    end
    E(i)=E(i)/N;
end
% Moins on a de bruit plus on est précis. Ce qui est logique

%% Etude de la méthode ARX
N=1000;
u=idinput(N);
M=idpoly(A,B,Ts);
y=sim(M,u);
z=[y u];
data=iddata(y,u,Ts);
n=[[2,2,1];[1,1,1];[0 0 0];[100 1 1];[1 100 1];[1 1 100]];
for i=1:length(n)
   sys=arx(z,n(i,:));
   figure()
   compare(data,sys);
end
% Pour [2,2,1] on obtient 100%
% Pour [1,1,1] on obtient 23.45%
% Pour [0 0 0] nb et nk ne peuvent être égale en même temps à 0 donc nk=1
% et on obtient environ 0%
% Pour na prédominant on a 100%
% Pour nb prédominant on a 100%
% Pour nk prédominant on a 0.3%
% En en déduit qu'il faut que nk>na et nk>nb
% Mais ce n'est pas intéressant de mettre un degré délirant quand [2 2 1]
% marche

%% Influence du bruit sur arx
e=[0.001 0.5 1 100];
n=[2,2,1];
for i=1:length(e)
    M=idpoly(A,B,1,1,1,e(i));
    y=sim(M,u,'Noise');
    z=[y u];
    data=iddata(y,u,Ts);
    sys=arx(z,n);
    yarx=sim(sys,u,'Noise');
    figure(i)
    subplot(2,1,1)
    plot(yarx);
    subplot(2,1,2)
    plot(y);
end
% Si on regarde la figure 1 et la figure 4 on voit bien que plus il y a de
% bruit moins yarx et y se ressemblent, logique, donc la méthode arx amplifie le bruit

[A1,B1,C1,D1,F1]=polydata(sys);
plot_handle=nyquistplot(sys);
showConfidence(plot_handle);
% On observe des rounds d'incertitude

%% Comparaison des méthodes ARX et ARMAX
C2= [1 -1.3 0.9];
D2= 1;
M=idpoly(A,B,C2,D2,1,'NoiseVariance',Ts);
y=sim(M,u,'Noise');
z=[y u];
data=iddata(y,u,Ts);
sys_arx=arx(z,n);
sys_armax=armax(z,[2 2 1 1]);
yarx=sim(sys_arx,u,'Noise');
yarmax=sim(sys_armax,u,'Noise');
figure()
    subplot(3,1,1)
    plot(y);
    subplot(3,1,2)
    plot(yarx);
    subplot(3,1,3)
    plot(yarmax);
