function [lambda_iq] = lambda(q,t, N_a)
    lambda_i = [];
    for i=1:N_a
        pow_init = i - 1;
        pow_val = max(i-1-q, 0);
        if i-1-q >= 0
            prec = factorial(pow_init) / factorial(i-1-q);
        else
            prec = 0;
        end
        lambda_iq(i) = prec*t^(pow_val);
    end
    
end

