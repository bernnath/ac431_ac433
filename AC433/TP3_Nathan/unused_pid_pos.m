function U = pid_pos(error, gains)
% gains [kp ki kd ; ...] as [gains_x ; gains_y ...]
    U = diag(gains(:,1)) * error + ...
        diag(gains(:,2)) * int(error) + ...
        diag(gains(:,3)) * diff(error);
end

