function [pos_dot_dot] = pos_dd_system_dynamics(T,phi, theta, psi_, m)
    constants;
    pos_dot_dot=[];
    pos_dot_dot(1) = T*(cos(phi)*sin(theta)*cos(psi_) + sin(phi)*sin(psi_))/m;
    pos_dot_dot(2) = T*(cos(phi)*sin(theta)*sin(psi_) - sin(phi)*cos(psi_))/m;
    pos_dot_dot(3) = T*(cos(phi)*cos(theta))/m - g;
end

