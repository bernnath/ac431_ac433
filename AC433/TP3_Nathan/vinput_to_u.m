function U = vinput_to_u(vinput)
    run('constants.m')
    v1 = vinput(1);
    v2 = vinput(2);
    v3 = vinput(3);
    T = m * sqrt(v1^2 + v2^2 + (v3 + g)^2);
    U(1) = T;
    U(2) = asin(m * (v1*sin(psi_)-v2*cos(psi_)) / T);
    U(3) = atan((v1*cos(psi_)-v2*sin(psi_)) / (v3 + g));
end

