function tfout = ss2tf2(ssin, iu, iout)
    [n, d] = ss2tf(ssin.A, ssin.B, ssin.C, ssin.D, iu);
    tfout = tf(n(iout,:), d);
end

