close all
clear all

%initializethesystemparameters
param.g=9.8;
param.h=0.05;
param.dblAlt=150;

%initializethetimestamp,thestateandinputreference:
xi_ref=[];
u_ref=[];
%definetheintialandfinalconditions:
xi= [0; 0; 0];
ui= [18; 0];
xf= [1000; 500; -pi/4];
uf= [18; 0];
T= 100;

%constructthematrixM\alpha=\bar{z}asineq.(6):
%%
der_nb = 3;
z_nb = 2;
m_size = der_nb*z_nb*2;
M = zeros(m_size);
for der_num=1:der_nb
    for z_num=1:z_nb
        i = der_num * z_num;
        j = (z_num-1) * m_size / 2;
        M(i,1:m_size / 2) = lambda(der_num-1, 0, m_size/2)
        M(2*i,j+1:end) = lambda(der_num-1, T, m_size/2);
    end
end
%%
M_=[1 0 0   0     0       0       0 0 0   0     0       0;
   0 1 0   0     0       0       0 0 0   0     0       0;
   0 0 2   0     0       0       0 0 0   0     0       0;
   0 0 0   0     0       0       1 0 0   0     0       0;
   0 0 0   0     0       0       0 1 0   0     0       0;
   0 0 0   0     0       0       0 0 2   0     0       0;
   1 T T^2 T^3   T^4     T^5     0 0 0   0     0       0;
   0 1 2*T 3*T^2 4*T^3   5*T^4   0 0 0   0     0       0;
   0 0 2   2*3*T 3*4*T^2 4*5*T^3 0 0 0   0     0       0;
   0 0 0   0     0       0       1 T T^2 T^3   T^4     T^5;
   0 0 0   0     0       0       0 1 2*T 3*T^2 4*T^3   5*T^4;
   0 0 0   0     0       0       0 0 2   2*3*T 3*4*T^2 4*5*T^3];


b=[xi(1) ui(1)*cos(xi(3)) -sin(xi(3))*param.g*tan(ui(2))...
   xi(2) ui(1)*sin(xi(3)) cos(xi(3))*param.g*tan(ui(2))...
   xf(1) uf(1)*cos(xf(3)) -sin(xf(3))*param.g*tan(uf(2))...
   xf(2) uf(1)*sin(xf(3)) cos(xf(3))*param.g*tan(uf(2))]';

a=inv(M)*b;

%definetheflatoutputanditsderivatives

z1=@(t)(a(1)+a(2)*t+a(3)*t.^2+a(4)*t.^3+a(5)*t.^4+a(6)*t.^5);
z2=@(t)(a(7)+a(8)*t+a(9)*t.^2+a(10)*t.^3+a(11)*t.^4+a(12)*t.^5);
zd1=@(t)(a(2)+2*a(3)*t+3*a(4)*t.^2+4*a(5)*t.^3+5*a(6)*t.^4);
zd2=@(t)(a(8)+2*a(9)*t+3*a(10)*t.^2+4*a(11)*t.^3+5*a(12)*t.^4);

zdd1=@(t)(2*a(3)+2*3*a(4)*t+3*4*a(5)*t.^2+4*5*a(6)*t.^3);
zdd2=@(t)(2*a(9)+2*3*a(10)*t+3*4*a(11)*t.^2+4*5*a(12)*t.^3);

X=@(t)([feval(z1,t);feval(z2,t);atan2(feval(zd2,t),feval(zd1,t))]);
U=@(t)([sqrt((feval(zd1,t).^2)+(feval(zd2,t).^2));...
atan2((feval(zdd2,t).*feval(zd1,t)-feval(zd2,t).*feval(zdd1,t)),...
(param.g.*(sqrt((feval(zd1,t).^2)+(feval(zd2,t).^2)))))]);

Vad=@(t)(1 ./ (2 * sqrt((feval(zd1,t).^2)+(feval(zd2,t).^2))) + 2.*feval(zd1,t).*feval(zdd1,t) + 2.*feval(zd2,t).*feval(zdd2,t) );
Psid=@(t)( (feval(zdd2,t).*feval(zd1,t) - feval(zd2,t).*feval(zdd1,t)) ./ (feval(zd1,t).^2 + feval(zd2,t).^2) );

tt=param.h:param.h:T;
xi_ref=[xi_ref feval(X,tt)];
u_ref=[u_ref feval(U,tt)];

Vad_ref = Vad(tt);
Psid_ref = Psid(tt);


subplot(3,3,[1 2 4 5])
plot(xi_ref(1,:), xi_ref(2,:))
axis equal
title({"Trajectoire Ci :" + " x_i=" + num2str(xi(1))...
    + " y_i=" + num2str(xi(2)) + " \psi_i=" + num2str(xi(3))...
    + " va_i=" + num2str(ui(1)) + " \theta_f=" + num2str(ui(2)), ...
    "Cf :" + " x_f=" + num2str(xf(1)) + " y_f=" + num2str(xf(2))...
    + " \psi_f=" + num2str(xf(3)) + " va_f=" + num2str(uf(1))...
    + " \theta_f=" + num2str(uf(2))    })

subplot(3,3,3)
plot(tt, xi_ref(1,:))
title("x = f(t)")

subplot(3,3,6)
plot(tt, xi_ref(2,:))
title("y = f(t)")

subplot(3,3,9)
plot(tt, xi_ref(3,:))
title("\psi = f(t)")
 
subplot(3,3,7)
plot(tt, u_ref(1,:))
title("Va = f(t)")

subplot(3,3,8)
plot(tt, u_ref(2,:))
title("\theta = f(t)")

figure;
subplot(2,1,1)
plot(tt, Vad_ref)
title({"Va' = f(t):" + " x_i=" + num2str(xi(1))...
    + " y_i=" + num2str(xi(2)) + " \psi_i=" + num2str(xi(3))...
    + " va_i=" + num2str(ui(1)) + " \theta_f=" + num2str(ui(2)), ...
    "Cf :" + " x_f=" + num2str(xf(1)) + " y_f=" + num2str(xf(2))...
    + " \psi_f=" + num2str(xf(3)) + " va_f=" + num2str(uf(1))...
    + " \theta_f=" + num2str(uf(2))    })

subplot(2,1,2)
plot(tt, Psid_ref)
title("\psi' = f(t)")


