
%% verif x" = v1 ...
vinput_cst = [0.1, 0.1, 0.1];
u_cst = vinput_to_u(vinput_cst);
pos_dd_cst = pos_dd_system_MIMO(u_cst);


%% PD test no wind
close all
clear all

pd_gains = [8, 6.2;
            8, 6.2;
            8, 6.2];

A = [ zeros(3) diag(ones(3, 1)); zeros(3, 6) ];
B = [zeros(3); diag(ones(3, 1))];
C = diag(ones(6,1));
D = zeros(6,6);

K = [pd_gains(1,1) 0             0             pd_gains(1, 2) 0              0;
     0             pd_gains(2,1) 0             0              pd_gains(2, 2) 0;
     0             0             pd_gains(3,1) 0              0              pd_gains(3, 2);
     ];
        
sys_lin_bf_pid = ss(A-B*K, B*K, C, D);

t = 0:1/1000:10;
x_ref = [ones(3, length(t)) ; zeros(3, length(t))]';
x_0 = zeros(6,1)';

figure();
lsim(sys_lin_bf_pid, x_ref, t, x_0);

tf_x = ss2tf2(sys_lin_bf_pid, 1, 1); 
figure();
margin(tf_x);
figure();
nyquist(tf_x);


%% PD test with wind
close all
% clear all

wind_speed = [0.1; 0.1; 0.1]; %m/s

pd_gains = [8, 6.2;
            8, 6.2;
            8, 6.2];
        
A = [ zeros(3) diag(ones(3, 1)); zeros(3, 6) ];
B = [zeros(3); diag(ones(3, 1))];
C = diag(ones(6,1));
D = zeros(6,6);

K = [pd_gains(1,1) 0             0             pd_gains(1, 2) 0              0;
     0             pd_gains(2,1) 0             0              pd_gains(2, 2) 0;
     0             0             pd_gains(3,1) 0              0              pd_gains(3, 2);
     ];

wind_B = [diag(ones(3,1)); zeros(3)];
wind_D = zeros(6,3);
 
sys_lin_bf_pid = ss(A-B*K, [B*K wind_B], C, [D wind_D]);

t = 0:1/1000:10;
x_ref = [ones(3, length(t)) ; zeros(3, length(t))]';
wind = (ones(3, length(t)) .* wind_speed)';
x_0 = zeros(6,1)';

figure();
lsim(sys_lin_bf_pid, [x_ref wind], t, x_0);

tf_x = ss2tf2(sys_lin_bf_pid, 1, 1); 
figure();
margin(tf_x);
figure();
nyquist(tf_x);


%% PID test with wind (non fonctionnel) 
close all
% clear all

wind_speed = [0.0; 0.0; 0.0]; %m/s

pid_gains = [10, 4.2, 1; %pdi
             10, 4.2, 1;
             10, 4.2, 1];
        
A_a = [ zeros(3) diag(ones(3, 1)) zeros(3); zeros(3, 9); diag(ones(3, 1)) zeros(3) diag(ones(3, 1))];
B_a = [zeros(3); diag(ones(3, 1)); zeros(3)];
C_a = [diag(ones(9,1))];
D_a = zeros(9,6);

K = [pid_gains(1,1) 0 0 pid_gains(1, 2) 0 0 pid_gains(1, 3) 0 0;
     0 pid_gains(2,1) 0 0 pid_gains(2, 2) 0 0 pid_gains(2, 3) 0;
     0 0 pid_gains(3,1) 0 0 pid_gains(3, 2) 0 0 pid_gains(3, 3)
     ];

wind_B = [diag(ones(3,1)); zeros(6,3)];
wind_D = zeros(9,6);

sys_lin_bf_pid = ss(A_a-B_a*K, [B_a*K wind_B], C_a, [D_a wind_D]);

t = 0:1/100:1000;
x_ref = [ones(3, length(t)) ; zeros(6, length(t))]';
wind = (ones(3, length(t)) .* wind_speed)';
x_0 = zeros(9,1)';

figure();
lsim(sys_lin_bf_pid, [x_ref wind], t, x_0);

tf_x = ss2tf2(sys_lin_bf_pid, 1, 1); 
figure();
margin(tf_x);
figure();
nyquist(tf_x);

