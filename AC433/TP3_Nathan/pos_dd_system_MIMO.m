function [pos_dot_dot] = pos_dd_system_MIMO(U)    
    constants;
    pos_dot_dot = pos_dd_system_dynamics(U(1),U(2), U(3), psi_, m);
end