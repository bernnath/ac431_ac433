%IP trajectory tracking for a UAV using differential flatness and feedback linearization
close all; clear all; clc

Kp=650;
Ki=300;
Kd=45;


% load the reference state (position, velocity and heading) 
% these are arbirarly given, you can give your own references (which you designed in Lab Session 1)
load Positionref
load Veloref
load Psiref

%% plot the reference position
figure; hold on; grid on
plot(positionref(2,:),positionref(3,:))
step=5;
%plots the direction of the UAV motion
quiver(positionref(2,1:step:end),positionref(3,1:step:end),dpositionref(1,1:step:end),dpositionref(2,1:step:end))
legend('reference trajectory', 'heading')
%%

%load the UAV simulink model
load_system('UAVFeedbackLinearize') 
set_param('UAVFeedbackLinearize', 'StopTime', num2str(positionref(1,end)))
sim('UAVFeedbackLinearize')


%%

% plot the position, velocity and the heading angle \Psi
figure; hold on; grid on
plot(position.signals.values(:,1),position.signals.values(:,2),'b')
legend('...', '...')
plot(position.signals.values(:,3),position.signals.values(:,4),'r--')
legend('...', '...')

figure; hold on; grid on
plot(psi.time, psi.signals.values(:,1),'b')
plot(psi.time, psi.signals.values(:,2),'r--')
legend('...', '...')

%% RMS

Kd_l = ;
Ki_l = logspace(0,1,10);
Kp_l = logspace(0,1,10);

res = [];
i = 0;
for Kd = Kd_l
    for Ki = Ki_l
        for Kp = Kp_l
            i = i+1
            P = [1 Kd Kp Ki];
            racine=roots(P);
            if all(racine<0)
                %load the UAV simulink model
                load_system('UAVFeedbackLinearize') 

                set_param('UAVFeedbackLinearize', 'StopTime', num2str(positionref(1,end)))
                try
                    sim('UAVFeedbackLinearize')
                catch
                end
                x = position.signals.values(:,1);
                xref = position.signals.values(:,3);
                epislon_x = xref - x;
                rms_x = rms(epislon_x);
                y = position.signals.values(:,2);
                yref = position.signals.values(:,4);
                epislon_y = yref - y;
                rms_y = rms(epislon_y);
                epsilon_n = sqrt((yref-y).^2+(xref-x).^2);
                rms_n = rms(epsilon_n);
                res = [res; Kd Kp Ki rms_x rms_y rms_n];
            end
        end
    end
end
res_sort = sortrows(res,6,"ascend");